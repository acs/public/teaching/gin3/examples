use crate::linked_list;
use core::alloc::Layout;
use core::cmp::{max, min};
use core::ptr::NonNull;

/// Minimal size of an allocated memory block
const MIN_ALLOC_SIZE: usize = 128;

#[derive(Debug)]
pub enum AllocatorError {
    OutOfMemory,
    TooBig,
}

#[derive(Debug)]
#[repr(align(64))]
pub struct BuddySystem<const ORDER: usize> {
    free_list: [linked_list::LinkedList; ORDER],
}

impl<const ORDER: usize> BuddySystem<ORDER> {
    /// Constructs the buddy system. `start` specifies the
    /// start address of the heap, while `len` specifies#
    /// the heap size.
    pub fn new(start: *mut u8, len: usize) -> Self {
        assert!((len & (len - 1)) == 0, "Heap size isn't a power of two");
        let order: usize = len.trailing_zeros().try_into().unwrap();
        assert!(order <= ORDER, "ORDER isn't large enough");

        let mut buddy_system = Self {
            free_list: [linked_list::LinkedList::new(); ORDER],
        };

        unsafe {
            buddy_system.free_list[order].push(start as *mut usize);
        }

        buddy_system
    }

    /// Allocates memory as described by the given `layout`.
    ///
    /// Returns as result a pointer to newly-allocated memory,
    /// or an error, which describes the reason of the error.
    pub fn alloc(&mut self, layout: Layout) -> Result<NonNull<u8>, AllocatorError> {
        let size = max(
            layout.size().next_power_of_two(),
            max(layout.align(), MIN_ALLOC_SIZE),
        );
        let order: usize = size.trailing_zeros().try_into().unwrap();

        if order >= ORDER {
            // size of the memory allocation is too large
            return Err(AllocatorError::TooBig);
        }

        for i in order..self.free_list.len() {
            // Find the first non-empty list, which handles a block of
            // memory with a equal or a larger size as the requested size
            if !self.free_list[i].is_empty() {
                // Split larger blocks in two buddies
                for j in (order + 1..i + 1).rev() {
                    if let Some(block) = self.free_list[j].pop() {
                        unsafe {
                            self.free_list[j - 1]
                                .push((block as usize + (1 << (j - 1))) as *mut usize);
                            self.free_list[j - 1].push(block);
                        }
                    } else {
                        return Err(AllocatorError::OutOfMemory);
                    }
                }

                if let Some(addr) = self.free_list[order].pop() {
                    return Ok(NonNull::new(addr as *mut u8).unwrap());
                } else {
                    return Err(AllocatorError::OutOfMemory);
                }
            }
        }

        Err(AllocatorError::OutOfMemory)
    }

    /// Deallocates the block of memory at the given `ptr` pointer with the given layout.
    pub fn dealloc(&mut self, ptr: NonNull<u8>, layout: Layout) {
        let size = max(
            layout.size().next_power_of_two(),
            max(layout.align(), MIN_ALLOC_SIZE),
        );
        let order: usize = size.trailing_zeros().try_into().unwrap();

        unsafe {
            // add block to free list
            self.free_list[order].push(ptr.as_ptr() as *mut usize);

            // Try to merge two buddies to one buddy
            let mut current_ptr = ptr.as_ptr() as usize;
            let mut current_order = order;

            'outer: while current_order < self.free_list.len() - 1 {
                let block_size = 1 << current_order;

                // the list is unordered => check all nodes to find a buddy
                for block in self.free_list[current_order].iter_mut() {
                    let buddy = block.value() as usize;
                    if buddy == current_ptr + block_size || buddy == current_ptr - block_size {
                        // remove current block from the list
                        block.remove();
                        // the first node of the list includes `ptr`
                        self.free_list[current_order].pop().unwrap();
                        // merge buddies
                        current_ptr = min(current_ptr, buddy);
                        current_order += 1;
                        self.free_list[current_order].push(current_ptr as *mut usize);
                        continue 'outer;
                    }
                }

                // no buddy merged => leave while loop
                break;
            }
        }
    }

    /// Print all elements of the lists
    pub fn dump_state(&self) {
        for order in (0..self.free_list.len()).rev() {
            if !self.free_list[order].is_empty() {
                print!("Block size {:>8}: ", 1 << order);

                for block in self.free_list[order].iter() {
                    print!("0x{:x} ", block as usize);
                }

                println!();
            }
        }
    }
}
