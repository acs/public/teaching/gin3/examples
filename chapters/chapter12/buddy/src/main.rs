mod buddy;
mod linked_list;

use crate::buddy::BuddySystem;
use core::alloc::Layout;

/// Size of the memory pool, which is managed by the buddy system.
///
/// The size must be a power of two.
const HEAP_SIZE: usize = 1024 * 1024;

#[repr(C, align(8))]
struct Heap([u8; HEAP_SIZE]);

impl Heap {
    pub const fn new() -> Self {
        Self([0; HEAP_SIZE])
    }

    pub fn as_ptr(&self) -> *const u8 {
        self.0.as_ptr()
    }

    pub fn as_mut_ptr(&mut self) -> *mut u8 {
        self.0.as_mut_ptr()
    }
}

static mut HEAP: Heap = Heap::new();

fn main() {
    println!("HEAP starts at 0x{:x}", unsafe { HEAP.as_ptr() as usize });

    let mut buddy = unsafe { BuddySystem::<32>::new(HEAP.as_mut_ptr(), HEAP_SIZE) };
    println!("\nPrint initial state of the buddy system:");
    buddy.dump_state();

    // Try to allocate 1024 bytes
    println!("\nTry to allocate memory...");
    let layout = Layout::from_size_align(1000, 8).unwrap();
    let ptr = buddy.alloc(layout).unwrap();
    println!("Got address 0x{:x}", ptr.as_ptr() as usize);

    println!(
        "\nPrint state of the buddy system after allocation of {} bytes:",
        layout.size()
    );
    buddy.dump_state();

    // Free allocated memory
    buddy.dealloc(ptr, layout);

    // Print state after deallocation
    println!(
        "\nPrint state of the buddy system after deallocation of {} bytes:",
        layout.size()
    );
    buddy.dump_state();
}
