# Buddy System

This is a simplified version of the [buddy_system_allocator](https://github.com/rcore-os/buddy_system_allocator).
It is used to explain the buddy system in the course _Foundations of Computer Science 3 - Operating Systems and System Security"_ (Grundgebiete der Informatik 3 - Betriebssysteme und Systemsicherheit) at the RWTH Aachen University, Germany.

## License

Some code is derived from phil-opp's linked-list-allocator and rcore-os' buddy_system_allocator.

Licensed under MIT License.