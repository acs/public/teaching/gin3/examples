
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#define NO_PHILOSOPHERS	5
pthread_mutex_t forks[NO_PHILOSOPHERS] = {[0 ... NO_PHILOSOPHERS-1] = PTHREAD_MUTEX_INITIALIZER };
void do_something();

static void* philosopher(void *arg) {
	size_t id = (size_t) arg;
	int left = id;
	int right = (id+1) % NO_PHILOSOPHERS;

	while(1) {
		printf("Philosopher %zd is thinking\n", id);
		do_something();

		printf("Philosopher %zd is taking forks\n", id);
		pthread_mutex_lock(&forks[left]);
		pthread_mutex_lock(&forks[right]);

		printf("Philosopher %zd is eating\n", id);
		do_something();

		printf("Philosopher %zd is putting forks down\n", id);
		pthread_mutex_unlock(&forks[right]);
		pthread_mutex_unlock(&forks[left]);
	}

	return 0;
}

void do_something() {
	size_t r = ((unsigned int)(((double) rand() / (double)RAND_MAX)*10))+1;

	usleep(r*500);
}

int main(int argc, char** argv) {
	pthread_t threads[NO_PHILOSOPHERS];

	// create philosophers
	for (size_t i = 0; i < NO_PHILOSOPHERS; i++) {
		pthread_create(&(threads[i]), NULL, philosopher, (void*)i);
	}

	sleep(5);

	printf("Stop dining philosophers\n");
	fflush(stdout);

	return 0;
}
