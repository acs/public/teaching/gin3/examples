#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char** argv) {
	// child process because return value zero
	if (fork() == 0)
		execlp("/bin/ls", "ls", "-la", NULL);
  	else // parent process because return value non-zero.
		wait(NULL);

	return 0;
}
