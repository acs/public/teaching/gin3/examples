#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char** argv) {
	// child process because return value zero
	if (fork() == 0)
		printf("Hello from child with id %d!\n", getpid());
  	else // parent process because return value non-zero.
		printf("Hello from parent with id %d!\n", getpid());

	return 0;
}
