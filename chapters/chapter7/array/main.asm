DEFAULT REL

SECTION .bss
	Matrix resd 16

SECTION .data
	msg db "Matrix[2][3] = %d", 10, 13, 0

SECTION .text

; define main as globale, which is recognizable by the linker  
global main
; define printf as external symbol
extern printf

ALIGN 8
init:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; save rbx and load the matrix address to rbx
	push rbx
	mov rbx, Matrix

	; set counter i to 0
	mov rcx, 0

Louter0:
	cmp rcx, 4
	jae Louter1

	; set count j to 0
	mov rdx, 0

Linner0:
	cmp rdx, 4
	jae Linner1

	; calculate i+j
	mov rax, rcx
	add rax, rdx

	; calculate index: rdi = rcx * no_elements
	imul rdi, rcx, 4
	add rdi, rdx
	
	mov DWORD [rbx + rdi * 4], eax

	inc rdx
	jmp Linner0
Linner1:
	inc rcx
	jmp Louter0
Louter1:

	; restore rbx
	pop rbx
	; restore rbp
	pop rbp

	ret

ALIGN 8
main:
	; create new stack frame
	push rbp
	mov rbp, rsp

	call init

	; print result
	mov rdi, msg
	mov rax, Matrix
	mov esi, DWORD [rax + 44] ; (2*4+3)*4=44
	call printf

	; restore rbp
	pop rbp

	; Leave program & signalize, that no error occurred during the execution
	mov rax, 0
	ret

%ifidn __OUTPUT_FORMAT__,elf64
SECTION .note.GNU-stack noalloc noexec nowrite progbits
%endif
