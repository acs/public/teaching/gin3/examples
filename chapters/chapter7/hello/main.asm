DEFAULT REL

SECTION .data
	msg db "Hello Wordl!", 10, 13, 0
	msg_len equ $ - msg
%ifidn __OUTPUT_FORMAT__, elf64
	__NR_write equ 1
%elifidn __OUTPUT_FORMAT__, macho64 
	__NR_write equ 0x2000004
%endif

SECTION .text

; define main as globale, which is recognizable by the linker  
global main

ALIGN 8
main:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; write(1, msg, strlen(msg));
	mov rdi, 1
	mov rsi, msg
	mov rdx, msg_len 
	mov rax, __NR_write
	syscall
	
	; restore rbp
	pop rbp

	mov rax, 0

	ret

%ifidn __OUTPUT_FORMAT__,elf64
section .note.GNU-stack noalloc noexec nowrite progbits
%endif
