DEFAULT REL

SECTION .data
	msg db "factorial = %d", 10, 13, 0

SECTION .text

; define main as globale, which is recognizable by the linker  
global main
; define printf as external symbol
extern printf

ALIGN 8
factorial:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; set counter to 1
	mov rcx, 1

	; create space for the local variable "fact"
	sub rsp, 8
	; set "fact" to the start value
	mov QWORD [rsp], 1

L0:
	; rdi has the value of the first argument
	; compare rdi with the count / rcx
	cmp rcx, rdi
	ja L1

	; copy the value of the local variable "fact" to rax
	mov rax, [rsp]
	; rdx:rax = rax * rcx
	mul rcx
	; copy result back to the local variable "fact"
	; we ignore the overflow
	mov QWORD [rsp], rax

	inc rcx
	jmp L0
L1:

	; copy result to the return value register / rax
	mov rax, [rsp]

	; remove local variable
	add rsp, 8

	; restore rbp
	pop rbp

	ret

ALIGN 8
main:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; dump register states
	mov rdi, 10
	call factorial

	; print result
	mov rdi, msg
	mov rsi, rax
	call printf

	; restore rbp
	pop rbp

	; Leave program & signalize, that no error occurred during the execution
	mov rax, 0
	ret

%ifidn __OUTPUT_FORMAT__,elf64
section .note.GNU-stack noalloc noexec nowrite progbits
%endif
