DEFAULT REL

SECTION .data
	msg db "RSI = %lld", 10, 13, 0

SECTION .text

; define main as globale, which is recognizable by the linker  
global main
; define printf as external symbol
extern printf

ALIGN 8
main:
	; create new stack frame
	push rbp
	mov rbp, rsp

	; dump register states
	mov rdi, msg
	mov rsi, 42
	call printf

	; restore old stack frame
	pop rbp

	; Leave program & signalize, that no error occurred during the execution
	mov rax, 0
	ret

%ifidn __OUTPUT_FORMAT__,elf64
section .note.GNU-stack noalloc noexec nowrite progbits
%endif
